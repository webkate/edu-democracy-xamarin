﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace Democracy
{
	public class App : MvxApplication
	{
		public App ()
		{
		}

		public override void Initialize ()
		{
			base.Initialize ();
			RegisterAppStart<GroupsViewModel> ();
		}
	}
}


﻿using System;
using Cirrious.MvvmCross.ViewModels;

namespace Democracy
{
	public class GroupsViewModel : MvxViewModel
	{
		public GroupsViewModel ()
		{
		}

		string name;
		public string Name {
			get {
				return name;
			}
			set {
				name = value;
				RaisePropertyChanged (() => Name);
			}
		}

	}
}


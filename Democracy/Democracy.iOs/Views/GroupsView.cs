﻿
using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Views;
using Cirrious.MvvmCross.Binding.BindingContext;

namespace Democracy.iOs
{
	public partial class GroupsView : MvxViewController
	{
		public GroupsView () : base ("GroupsView", null)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Perform any additional setup after loading the view, typically from a nib.
			var label = new UILabel(new RectangleF(110,70,100,54));
			//			Add (label);
			var edit = new UITextView(new RectangleF(10,70,100,54));

			Add (label);
			Add (edit);

			GroupsViewModel group = new GroupsViewModel ();
			group.Name = "Some predefined";

			this.CreateBinding (label).To ((GroupsViewModel v) => v.Name).Apply ();
			this.CreateBinding (edit).To ((GroupsViewModel v) => v.Name).Apply ();




		}
	}

}


﻿using System;
using Cirrious.MvvmCross.Touch.Platform;
using Democracy;
using MonoTouch.UIKit;

namespace Democracy.iOS
{
	public class Setup : MvxTouchSetup
	{
		public Setup (MvxApplicationDelegate del, UIWindow window):base(del,window)
		{
		}

		#region implemented abstract members of MvxSetup

		protected override Cirrious.MvvmCross.ViewModels.IMvxApplication CreateApp ()
		{
			return new App ();
		}

		#endregion
	}
}

